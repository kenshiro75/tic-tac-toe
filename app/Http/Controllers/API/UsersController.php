<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserKey;
use App\Services\FattureInCloudService;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use PDOException;

class UsersController extends Controller
{
    public function check(Request $request)
    {
        try {
            if (empty($request->user()))
            {
                throw new AuthenticationException('User not found');
            }

            if (empty($request->uid))
            {
                throw ValidationException::withMessages(['uid' => 'The uid field is required.']);
            }

            if (is_null($request->user()->key))
            {
                throw new AuthenticationException('User has no key with this UID');
            }

            $fic = new FattureInCloudService($request->user());
            $response = $fic->apiRequest('GET',config('fattureincloud.api.tabelle.service'));
            $body = json_decode($response->getBody(),true);

            if ($response->getStatusCode()!=200||array_key_exists('error',$body))
            {
                throw new Exception($response->getBody(),($response->getStatusCode()==200) ? 403 : $response->getStatusCode());
            }

            return response()->json(['status' => 'ok', 'data' => $body]);



        }
        catch (AuthenticationException $a)
        {
            return response()->json(['status' => 'ko', 'errors' => $a->getMessage()],403);
        }
        catch(ValidationException $ev)
        {
            return response()->json(['status' => 'ko', 'errors' => $ev->errors()],400);
        }
        catch(PDOException $ep)
        {
            Log::error('class: '.__CLASS__.' - file: '.$ep->getFile().' - line:'.$ep->getLine(),[$ep->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => 'data error'],403);
        }
        catch(Exception $e)
        {
            Log::error('class: '.__CLASS__.' - file: '.$e->getFile().' - line:'.$e->getLine(),[$e->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => json_decode($e->getMessage(),true)],$e->getCode() ?? 500);
        }

    }

    public function add(Request $request)
    {
        try {
            if (empty($request->user()))
            {
                throw new AuthenticationException('User not found');
            }

            $validator = Validator::make($request->all(), [
                'api_key' => 'required|string',
                'uid' => 'required|string',
                'uri' => 'required|string'
            ]);

            if ($validator->passes())
            {

                UserKey::create([
                    'api_key' => $request->api_key,
                    'uid' => $request->uid,
                    'uri' => $request->uri,
                    'user_id' => $request->user()->id
                ]);

                return response()->json(['status' => 'ok']);
            }

            throw new ValidationException($validator);
        }
        catch (AuthenticationException $a)
        {
            return response()->json(['status' => 'ko', 'errors' => $a->getMessage()],403);
        }
        catch(ValidationException $ev)
        {
            return response()->json(['status' => 'ko', 'errors' => $ev->errors()],400);
        }
        catch(PDOException $ep)
        {
            Log::error('class: '.__CLASS__.' - file: '.$ep->getFile().' - line:'.$ep->getLine(),[$ep->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => 'data error'],403);
        }
        catch(Exception $e)
        {
            Log::error('class: '.__CLASS__.' - file: '.$e->getFile().' - line:'.$e->getLine(),[$e->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => $e->getMessage()],500);
        }

    }

    public function serviceTables(Request $request)
    {
        try {
            if (empty($request->user()))
            {
                throw new AuthenticationException('User not found');
            }

            if (empty($request->uid))
            {
                throw ValidationException::withMessages(['uid' => 'The uid field is required.']);
            }

            if (is_null($request->user()->key))
            {
                throw new AuthenticationException('User has no key with this UID');
            }

            $fields = [
                "campi" => [
                    "lista_valute",
                    "lista_iva",
                    "lista_metodi_pagamento",
                    "lista_paesi"
                ]
            ];

            $fic = new FattureInCloudService($request->user());
            $response = $fic->apiRequest('GET',config('fattureincloud.api.tabelle.info'),$fields);
            $body = json_decode($response->getBody(),true);

            if ($response->getStatusCode()!=200||array_key_exists('error',$body))
            {
                throw new Exception($response->getBody(),($response->getStatusCode()==200) ? 403 : $response->getStatusCode());
            }

            if(array_key_exists('success',$body))
            {
                unset($body['success']);
            }

            return response()->json(['status' => 'ok', 'data' => $body]);

        }
        catch (AuthenticationException $a)
        {
            return response()->json(['status' => 'ko', 'errors' => $a->getMessage()],403);
        }
        catch(ValidationException $ev)
        {
            return response()->json(['status' => 'ko', 'errors' => $ev->errors()],400);
        }
        catch(PDOException $ep)
        {
            Log::error('class: '.__CLASS__.' - file: '.$ep->getFile().' - line:'.$ep->getLine(),[$ep->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => 'data error'],403);
        }
        catch(Exception $e)
        {
            Log::error('class: '.__CLASS__.' - file: '.$e->getFile().' - line:'.$e->getLine(),[$e->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => json_decode($e->getMessage(),true)],$e->getCode() ?? 500);
        }

    }

}