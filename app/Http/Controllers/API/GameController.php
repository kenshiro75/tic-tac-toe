<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Move;
use App\Models\User;
use App\Models\UserKey;
use App\Services\FattureInCloudService;
use App\Services\GameService;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use PDOException;

class GameController extends Controller
{
    public function start(Request $request)
    {
        try {
            if (empty($request->user()))
            {
                throw new AuthenticationException('User not found');
            }

            $game = new Game();
            $game->player_one_id = $request->user()->id;
            $game->save();


            return response()->json(['status' => 'ok', 'id' => $game->id, 'player_symbol' => $game->playerOneSymbol()]);

        }
        catch (AuthenticationException $a)
        {
            return response()->json(['status' => 'ko', 'errors' => $a->getMessage()],403);
        }
        catch(Exception $e)
        {
            Log::error('class: '.__CLASS__.' - file: '.$e->getFile().' - line:'.$e->getLine(),[$e->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => json_decode($e->getMessage(),true)],$e->getCode() ?? 500);
        }

    }

    public function move(Request $request)
    {

        try {
            if (empty($request->user()))
            {
                throw new AuthenticationException('User not found');
            }

            $validator = Validator::make($request->all(), [
                'matrix_idx' => 'required|integer|between:1,9',
                'game_id'  => 'required|numeric',
            ]);

            if ($validator->passes())
            {

                // check game exists
                $game = Game::find($request->game_id);
                if(is_null($game))
                {
                    throw new Exception('game not found!');
                }

                if($game->status != Game::OPEN)
                {
                    return response()->json(['status' => 'ko', 'game_statistics' => GameService::statistics($game)],403);
                }

                $player = User::find($request->user()->id);

                // check player is one of the game players
                if(!GameService::checkPlayer($game,$player))
                {
                    throw new Exception('this player cannot play this game!');
                }

                // if player not in game add it
                if(is_null($game->player_two_id)&&($game->player_one_id!=$player->id))
                {
                    $game->player_two_id = $player->id;
                    $game->save();
                }

                // check player can move
                if(!GameService::checkPlayerTurn($game,$player))
                {
                    throw new Exception('this player cannot move this time!');
                }

                // check player can move
                if(!GameService::checkMoveIsAvailable($game,$request->matrix_idx))
                {
                    throw new Exception('this move is not possible!');
                }

                // add new move
                $move = new Move();
                $move->player_id = $player->id;
                $move->matrix_idx = $request->matrix_idx;
                $move->game_id = $request->game_id;
                $move->save();

                // check game status and eventually update game
                $gameStatus = GameService::getGameStatus($game);

                if ($gameStatus!=Game::OPEN)
                {
                    $game->status = $gameStatus;
                    $game->save();
                }


                return response()->json(['status' => 'ok', 'game_statistics' => GameService::statistics($game)]);
            }

            throw new ValidationException($validator);
        }
        catch (AuthenticationException $a)
        {
            return response()->json(['status' => 'ko', 'errors' => $a->getMessage()],403);
        }
        catch(ValidationException $ev)
        {
            return response()->json(['status' => 'ko', 'errors' => $ev->errors()],400);
        }
        catch(PDOException $ep)
        {
            Log::error('class: '.__CLASS__.' - file: '.$ep->getFile().' - line:'.$ep->getLine(),[$ep->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => 'data error'],403);
        }
        catch(Exception $e)
        {
            Log::error('class: '.__CLASS__.' - file: '.$e->getFile().' - line:'.$e->getLine(),[$e->getMessage()]);
            return response()->json(['status' => 'ko', 'errors' => $e->getMessage()],500);
        }

    }


}