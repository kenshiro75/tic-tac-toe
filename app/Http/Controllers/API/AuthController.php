<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\CustomerService;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    public function signin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
            'password' => 'required'
        ]);

        if ($validator->passes())
        {
            if (!Auth::attempt([
                'email' => $request->email,
                'password' => $request->password
            ])) {

                return response()->json(['status' => 'ko', 'errors' => 'not valid credentials'],403);
            }

            $token = Str::random(60);

            Auth::user()->forceFill([
                'api_token' => hash('sha256', $token),
            ])->save();


            return response()->json(['status' => 'ok', 'bearer' => $token],200);
        }
        else {
            return response()->json(['status' => 'ko', 'errors' => $validator->errors()],403);
        }
    }

}