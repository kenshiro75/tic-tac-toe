<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Log;

Class Maintenance extends Middleware {

    public function handle($request, Closure $next, ... $roles)
    {

        $setting = Setting::where('key','website_status')->first();

        if (is_null($setting)||($setting->value!='0')||(strstr($request->route()->getName(),'login')!=false))
        {
            return $next($request);
        }

        return redirect(route('fe.maintenance'));
    }

}
