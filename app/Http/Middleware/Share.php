<?php

namespace App\Http\Middleware;

use App\Models\Company;
use App\Models\Content;
use App\Models\Customer;
use App\Models\OrderHeader;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Component;
//use App\Services\ContentService;
use App\Services\ContentService;
use App\Services\LanguageService;
use App\Services\TranslationService;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;


Class Share extends Middleware
{

    public function handle($request, Closure $next, ... $roles)
    {
//        if (!Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
//            return redirect('login-view');

        // current route name for menù binding
        $pageName = $request->route()->getName();

        $settings = [];
        foreach (Setting::all() as $setting)
        {
            $settings[$setting->key] = $setting->value;
        }

        $company = Company::find(1) ?? new Company();


        $currentLanguage = LanguageService::getCurrentFromRequest($request);

//        Log::debug('======= SHARE currentLanguage ==>',[$currentLanguage]);

        // header section
        $headerSections = [];
        $pageHeader = Content::where('key','page_header')->first();
        if (!is_null($pageHeader)&&!is_null($pageHeader->{$currentLanguage}))
        {

            $headerPages = json_decode($pageHeader->{$currentLanguage})->header->pages;

            $headerSections = array_map(function($item){
                return $item[0]->value;
            },$headerPages);

        }

        if (Auth::check()&&Auth::user()->isCustomer())
        {
            $cart = OrderHeader::where(['status'=>0,'customer_id'=>Customer::where('user_id',Auth::user()->id)->first()->id])->first();
            if(!is_null($cart))
            {
                foreach ($cart->rows as &$row)
                {
                    $row->product_name = $row->product->name->{$currentLanguage};
                }
            }


            View::share('cart',$cart);
        }

        $general = (object) [

            'translation' => TranslationService::getLanguangeSet($currentLanguage),

        ];


        View::share('general',$general);
        View::share('settings',$settings);
        View::share('company',$company);
        View::share('user', Auth::user());
        View::share('headerSections', $headerSections);
        View::share('components', Component::whereNotIn('name',['header','footer'])->get());
        // View::share('browserLanguage',$browserLanguage);
        View::share('currentLanguage',$currentLanguage);



        return $next($request);


    }
}
