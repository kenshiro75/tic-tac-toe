<?php

namespace App\Dtos;

class DocumentDto
{
    public ?string $id;                         // (string, opzionale): Identificativo univoco del documento da modificare (ha priorità sul token),
    public ?string $token;                      // (string, opzionale): Identificativo permanente del documento (utilizzato per identificare il documento solo se manca il parametro "id", non viene sovrascritto),
    public ?string $id_cliente;                 // (string, opzionale): Identificativo univoco del cliente (serve per creare un collegamento tra il documento e un cliente in anagrafica; se nullo, il documento non viene collegato a nessun cliente già esistente in anagrafica; se mancante, viene fatto il collegamento con piva o cf) [solo con tipo!="ordforn"]
    public ?string $id_fornitore;               // (string, opzionale): Identificativo univoco del fornitore (serve per creare un collegamento tra il documento e un fornitore in anagrafica; se nullo, il documento non viene collegato a nessun fornitore già esistente in anagrafica; se mancante, viene fatto il collegamento con piva o cf) [solo con tipo="ordforn"],
    public string $nome;                        // (string): Nome o ragione sociale del cliente/fornitore,
    public ?string $indirizzo_via;              // (string, opzionale): Indirizzo del cliente,
    public ?string $indirizzo_cap;              // (string, opzionale): CAP del cliente/fornitore,
    public ?string $indirizzo_citta;            // (string, opzionale): Città (comune) del cliente/fornitore,
    public ?string $indirizzo_provincia;        // (string, opzionale): Provincia del cliente/fornitore,
    public ?string $indirizzo_extra;            // (string, opzionale): Note extra sull'indirizzo,
    public ?string $paese;                      // (string, opzionale): Paese (nazionalità) del cliente/fornitore,
    public ?string $paese_iso;                  // (string, opzionale): [Solo se "paese" non è valorizzato] Codice ISO del paese (nazionalità) del cliente/fornitore (formato ISO alpha-2) in alternativa al parametro "paese",
    public ?string $lingua;                     // (string, opzionale): Lingua del documento (sigla) = ['it' o 'en' o 'de'],
    public ?string $piva;                       // (string, opzionale): Partita IVA cliente/fornitore; viene utilizzata per ricercare e collegare il cliente/fornitore in anagrafica se non specificato il parametro id_cliente/id_fornitore (in caso di doppioni viene collegato solo un soggetto),
    public ?string $cf;                         // (string, opzionale): Codice fiscale cliente/fornitore; viene utilizzato per ricercare e collegare il cliente/fornitore in anagrafica se non specificati i parametri id_cliente/id_fornitore e piva (in caso di doppioni viene collegato solo un soggetto),
    public ?bool $autocompila_anagrafica;       // (boolean, opzionale): Se "true", aggiorna l'anagrafica clienti/fornitori con i dati anagrafici della fattura: se il cliente/fornitore non esiste viene creato, mentre se il cliente/fornitore esiste già i dati vengono aggiornati; il cliente/fornitore viene ricercato tramite i campi id_cliente/id_fornitore, piva e cf (in quest'ordine),
    public ?bool $salva_anagrafica;             // (boolean, opzionale): Se "true", aggiorna l'anagrafica clienti/fornitori con i dati anagrafici della fattura: se il cliente/fornitore non esiste viene creato, mentre se il cliente/fornitore esiste già i dati vengono aggiornati; il cliente/fornitore viene ricercato tramite i campi id_cliente/id_fornitore, piva e cf (in quest'ordine),
    public ?string $numero;                     // (string, opzionale): Numero (e serie) del documento; se mancante viene utilizzato il successivo proposto per la serie principale; se viene specificata solo la serie (stringa che inizia con un carattere non numerico) viene utilizzato il successivo per quella serie; per i ddt agisce anche sul campo "ddt_numero" e non accetta una serie,
    public ?string $data;                       // (date, opzionale): Data di emissione del documento (per i ddt agisce anche sul campo "ddt_data"),
    public ?string $valuta;                     // (string, opzionale): Valuta del documento e degli importi indicati,
    public ?float $valuta_cambio;               // (double, opzionale): Tasso di cambio EUR/{valuta} [Se non specificato viene utilizzato il tasso di cambio odierno],
    public ?bool $prezzi_ivati;                 // (boolean, opzionale): Specifica se i prezzi da utilizzare per il calcolo del totale documento sono quelli netti, oppure quello lordi (comprensivi di iva),
    public ?float $rivalsa;                     // (double, opzionale): [Non presente in ddt e ordforn] Percentuale rivalsa INPS,
    public ?float $cassa;                       // (double, opzionale): [Non presente in ddt e ordforn] Percentuale cassa previdenziale,
    public ?float $rit_acconto;                 // (double, opzionale): [Non presente in ddt e ordforn] Percentuale ritenuta d'acconto,
    public ?float $imponibile_ritenuta;         // (double, opzionale): [Non presente in ddt e ordforn] Imponibile della ritenuta d'acconto (percentuale sul totale),
    public ?float $rit_altra;                   // (double, opzionale): [Non presente in ddt e ordforn] Percentuale altra ritenuta (ritenuta previdenziale, Enasarco etc.),
    public ?float $marca_bollo;                 // (double, opzionale): [Non presente in ddt e ordforn] Valore della marca da bollo (0 se non presente),
    public ?string $oggetto_visibile;           // (string, opzionale): [Non presente in ddt] Oggetto mostrato sul documento (precedentemente "oggetto_fattura"),
    public ?string $oggetto_interno;            // (string, opzionale): [Non presente in ddt] Oggetto (per organizzazione interna),
    public ?string $centro_ricavo;              // (string, opzionale): [Non presente in ddt e ordforn] Centro ricavo,
    public ?string $centro_costo;               // (string, opzionale): [Solo in ordforn] Centro di costo,
    public ?string $note;                       // (string, opzionale): [Non presente in ddt] Note (in formato HTML),
    public ?bool $nascondi_scadenza;            // (boolean, opzionale): [Non presente in ddt] Nasconde o mostra la scadenza sul documento,
    public ?bool $ddt;                          // (boolean, opzionale): [Non presente in ndc e ordforn] Indica la presenza di un DDT incluso nel documento (per i ddt è sempre true),
    public ?bool $ftacc;                        // (boolean, opzionale): [Solo se tipo=fatture] Indica la presenza di una fattura accompagnatoria inclusa nel documento,
    public ?string $id_template;                // (string, opzionale): [Solo se tipo!=ddt] Identificativo del template del documento [Se non specificato o inesistente, viene utilizzato quello di default],
    public ?string $ddt_id_template;            // (string, opzionale): [Solo se ddt=true] Identificativo del template del ddt [Se non specificato o inesistente, viene utilizzato quello di default],
    public ?string $ftacc_id_template;          // (string, opzionale): [Solo se ftacc=true] Identificativo del template della fattura accompagnatoria [Se non specificato o inesistente, viene utilizzato quello di default],
    public ?bool $mostra_info_pagamento;        // (boolean, opzionale): [Non presente in ddt e ndc] Mostra o meno le informazioni sul metodo di pagamento sul documento,
    public ?string $metodo_pagamento;           // (string, opzionale): [Solo se mostra_info_pagamento=true] Nome del metodo di pagamento,
    public ?string $metodo_titoloN;             // (string, opzionale): [Solo se mostra_info_pagamento=true] Titolo della riga N del metodo di pagamento (N da 1 a 5),
    public ?string $metodo_descN;               // (string, opzionale): [Solo se mostra_info_pagamento=true] Descrizione della riga N del metodo di pagamento (N da 1 a 5),
    public ?string $mostra_totali;              // (string, opzionale): [Solo per preventivi, rapporti e ordforn] Nasconde o mostra la scadenza sul documento = ['tutti' o 'netto' o 'nessuno'],
    public ?bool $mostra_bottone_paypal;        // (boolean, opzionale): [Solo per ricevute, fatture, proforma, ordini] Mostra il bottone "Paga con Paypal",
    public ?bool $mostra_bottone_ts_pay;        // (boolean, opzionale): [Solo per fatture, proforma, ricevute] Mostra il bottone "Paga con TS Pay",
    public ?bool $mostra_bottone_bonifico;      // (boolean, opzionale): [Solo per ricevute, fatture, proforma, ordini] Mostra il bottone "Paga con Bonifico Immediato",
    public ?bool $mostra_bottone_notifica;      // (boolean, opzionale): [Solo per ricevute, fatture, proforma, ordini] Mostra il bottone "Notifica pagamento effettuato",
    public ?array $lista_articoli;              // (Array[DocNuovoArticolo]): Lista degli articoli/righe del documento,
    public ?array $lista_pagamenti;             // (Array[DocNuovoPagamento]): [Non presente in preventivi, ddt e ordforn] Lista delle tranches di pagamento,
    public ?string $ddt_numero;                 // (string, opzionale): [Se ddt=true] Numero del ddt (se tipo="ddt" corrisponde al campo "numero") [Se non specificato viene autocompletato],
    public ?string $ddt_data;                   // (date, opzionale): [Se ddt=true] Data del ddt Numero del ddt [Obbligatoria solo se e solo se il documento non è un ddt ma ddt=true],
    public ?string $ddt_colli;                  // (string, opzionale): [Se ddt/ftacc=true] Numero di colli specificato nel ddt,
    public ?string $ddt_peso;                   // (string, opzionale): [Se ddt/ftacc=true] Peso specificato nel ddt,
    public ?string $ddt_causale;                // (string, opzionale): [Se ddt/ftacc=true] Causale specificata nel ddt,
    public ?string $ddt_luogo;                  // (string, opzionale): [Se ddt/ftacc=true] Luogo di spedizione specificato nel ddt,
    public ?string $ddt_trasportatore;          // (string, opzionale): [Se ddt/ftacc=true] Dati trasportatore specificati nel ddt,
    public ?string $ddt_annotazioni;            // (string, opzionale): [Se ddt/ftacc=true] Annotazioni specificate nel ddt,
    public ?bool $PA;                           // (boolean, opzionale): [Solo per fatture e ndc elettroniche] Indica se il documento è nel formato FatturaPA; se "true", vengono presi in considerazione tutti i successivi campi con prefisso "PA_", con eventuali eccezioni (se non valorizzati, vengono utilizzati i valori di default),
    public ?string $PA_tipo_cliente;            // (string, opzionale): [Solo se PA=true] Indica la tipologia del cliente: Pubblica Amministrazione ("PA") oppure privato ("B2B") = ['PA' o 'B2B'],
    public ?string $PA_tipo;                    // (string, opzionale): [Solo se PA=true] Tipo di documento a cui fa seguito la fattura/ndc in questione = ['ordine' o 'convenzione' o 'contratto' o 'nessuno'],
    public ?string $PA_numero;                  // (string, opzionale): [Solo se PA=true] Numero del documento a cui fa seguito la fattura/ndc in questione,
    public ?string $PA_data;                    // (date, opzionale): [Solo se PA=true] Data del documento a cui fa seguito la fattura/ndc in questione,
    public ?string $PA_cup;                     // (string, opzionale): [Solo se PA_tipo_cliente=PA] Codice Unitario Progetto,
    public ?string $PA_cig;                     // (string, opzionale): [Solo se PA_tipo_cliente=PA] Codice Identificativo della Gara,
    public ?string $PA_codice;                  // (string, opzionale): [Solo se PA=true] Codice Ufficio della Pubblica Amministrazione o Codice Destinatario,
    public ?string $PA_pec;                     // (string, opzionale): [Solo se PA_tipo_cliente=B2B] Indirizzo PEC del destinatario, utilizzato in assenza di Codice Destinatario,
    public ?string $PA_esigibilita;             // (string, opzionale): [Solo se PA=true] Esigibilità IVA e modalità di versamento (I=immediata, D=differita, S=split payment, N=non specificata) = ['I' o 'D' o 'S' o 'N'],
    public ?string $PA_modalita_pagamento;      // (string, opzionale): [Solo se PA=true] Modalità di pagamento (vedi tabella codifiche sulla documentazione ufficiale),
    public ?string $PA_istituto_credito;        // (string, opzionale): [Solo se PA=true] Nome dell'istituto di credito,
    public ?string $PA_iban;                    // (string, opzionale): [Solo se PA=true] Codice IBAN del conto corrente del beneficiario,
    public ?string $PA_beneficiario;            // (string, opzionale): [Solo se PA=true] Beneficiario del pagamento,
//    public ?string $extra_anagrafica;           // (DocNuovoExtraAnagrafica, opzionale): Informazioni anagrafiche aggiuntive da associare al cliente/fornitore [solo se salva_anagrafica=true],
    public ?bool $split_payment;                // (boolean, opzionale): [Solo per fatture, ndc e proforma NON elettroniche] Specifica se il documento applica lo split payment

    public function __construct(object $data)
    {
        foreach ($data as $k => $v)
        {
            $this->{$k} = $v;
        }
    }

}