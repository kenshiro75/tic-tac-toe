<?php

namespace App\Dtos;

class ProductDto
{
/*
  "cod": "",
  "nome": "Prodotto esempio",
  "desc": "",
  "prezzo_ivato": false,
  "prezzo_netto": "0.00",
  "prezzo_lordo": "0.00",
  "costo": "0.00",
  "cod_iva": 0,
  "um": "",
  "categoria": "",
  "note": "",
  "magazzino": false,
  "giacenza_iniziale": "0"
 */
//    public ?int $id;
    public ?string $code;
    public string $nome;
    public ?string $desc;
    public ?bool $prezzo_ivato;
    public ?float $prezzo_netto;
    public ?float $prezzo_lordo;
    public ?float $costo;
    public ?int $cod_iva;
    public ?string $um;
    public ?string $categoria;
    public ?string $note;
    public ?bool $magazzino;
    public ?int $giacenza_iniziale;

    public function __construct(object $data)
    {
        foreach ($data as $k => $v)
        {
            $this->{$k} = $v;
        }

        if (!empty($data->prezzo_ivato)&&($data->prezzo_ivato==true))
        {
            unset($this->prezzo_netto);
        }

        if (!empty($data->prezzo_ivato)&&($data->prezzo_ivato==false))
        {
            unset($this->prezzo_lordo);
        }

        if (empty($data->magazzino)||($data->magazzino==false))
        {
            unset($this->giacenza_iniziale);
        }
    }

}