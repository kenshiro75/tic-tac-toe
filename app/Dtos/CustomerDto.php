<?php

namespace App\Dtos;

class CustomerDto
{
    public ?int $id;
    public string $nome;
    public ?string $referente;
    public ?string $indirizzo_via;
    public ?string $indirizzo_cap;
    public ?string $indirizzo_citta;
    public ?string $indirizzo_provincia;
    public ?string $indirizzo_extra;
    public ?string $paese;
    public ?string $paese_iso;
    public ?string $mail;
    public ?string $tel;
    public ?string $fax;
    public ?string $piva;
    public ?string $cf;
    public ?int $termini_pagamento;
    public ?bool $pagamento_fine_mese;
    public ?int $cod_iva_default;
    public ?string $extra;
    public ?bool $PA;
    public ?string $PA_codice;

    public function __construct(object $data)
    {
        foreach ($data as $k => $v)
        {
            $this->{$k} = $v;
        }
    }

}