<?php

namespace App\Dtos;

class DocumentPaymentDto
{
    public string $data_scadenza;   // (date): Data di scadenza del pagamento
    public string $metodo;          // (string): Medodo di pagamento = ['not' o 'rev' o il nome del conto] ('not' indica che non è stato saldato, 'rev' che è stato stornato; se non esiste un conto con il nome specificato viene creato un nuovo conto in FIC),
    public string $importo;         // (string): Importo del pagamento (se vale 0 la tranche di pagamento non viene inserita; se vale "auto" e la tranche è una sola viene completato automaticamente),
    public ?string $data_saldo;     // (date, opzionale): Data del saldo dell'importo indicato [Obbligatorio se metodo!="not"]


    public function __construct(object $data)
    {
        foreach ($data as $k => $v)
        {
            $this->{$k} = $v;
        }
    }

}