<?php

namespace App\Dtos;

class DocumentItemDto
{
    public ?string $id;                     // (string, opzionale): Identificativo del prodotto (se nullo o mancante, la registrazione non viene collegata a nessun prodotto presente nell'elenco prodotti),
    public string $codice;                  // (string, opzionale): Codice prodotto,
    public string $nome;                    // (string, opzionale): Nome articolo,
    public ?string $um;                     // (string, opzionale): Unità di misura per il prodotto,
    public ?float $quantita;                // (double, opzionale): Quantità di prodotto Default:	1
    public ?string $descrizione;            // (string, opzionale): Descrizione del prodotto,
    public ?string $categoria;              // (string, opzionale): Categoria del prodotto (utilizzata per il piano dei conti),
    public ?float $prezzo_netto;            // (double, opzionale): Prezzo unitario netto (IVA esclusa) [Obbligatorio se prezzi_netti!=true],
    public ?float $prezzo_lordo;            // (double, opzionale): Prezzo unitario lordo (comprensivo di IVA) [Obbligatorio se prezzi_ivati=true],
    public int $cod_iva;                   // (number): Codice aliquota IVA (ottenibili con il parametro "lista_iva" della funzione "/info/account"),
    public ?bool $tassabile;                // (boolean, opzionale): Indica se l'articolo è imponibile,
    public ?float $sconto;                  // (double, opzionale): Sconto (percentuale),
    public ?bool $applica_ra_contributi;    // (boolean, opzionale): Indica se a questo articolo vengono applicate ritenute e contributi,
    public ?int $sconto_rosso;              // (integer, opzionale): Ordine dell'articolo nel documento (ordinamento ascendente da 0 in poi),
    public ?int $ordine;                    // (integer, opzionale): Se vale 1, evidenzia in rosso l'eventuale sconto in fattura = ['0' o '1'],
    public ?bool $in_ddt;                   // (boolean, opzionale): Indica se l'articolo è incluso nel ddt (se presente un ddt allegato, altrimenti non è significativo),
    public ?bool $magazzino;                // (boolean, opzionale): Indica se viene movimentato il magazzino (true: viene movimentato; false: non viene movimentato) [Non influente se il prodotto non è collegato all'elenco prodotti, oppure la funzionalità magazzino è disattivata]


    public function __construct(object $data)
    {
        foreach ($data as $k => $v)
        {
            $this->{$k} = $v;
        }
    }

}