<?php


namespace App\Services;


use App\Models\Game;
use App\Models\User;


class GameService
{

    // winner combinations
    public static $matrixIdxsCombinationToWin = [
        [1,2,3],[4,5,6],[7,8,9],
        [1,4,7],[2,5,8],[3,6,9],
        [1,5,9],[3,5,7]
    ];

    public static function checkPlayer(Game $game, User $player): bool
    {
        return ((!is_null($game->player_one_id))&&($game->player_one_id == $player->id)) || ((!is_null($game->player_two_id))&&($game->player_two_id == $player->id)) || (empty($game->player_two_id));
    }

    /**
     * @param Game $game
     * @return int
     */
    public static function getGameStatus(Game $game): int
    {

        // cannot set a complete sequence before (3+2) moves in a game
        if($game->moves()->count()<5)
        {
            return Game::OPEN;
        }

        // game has max 9 moves
        if($game->moves()->count()>9)
        {
            return Game::ERROR;
        }

        // after the 5th move check possible winner combination
        if (!self::getWinnerPlayerId($game))
        {
            // no winner combination
            // moves < 9 == still playing == OPEN
            // moves == 9 == game ended with no winner == DRAW
            return ($game->moves()->count()==9) ? Game::DRAW : Game::OPEN;
        }


        return  Game::WON;

    }

    /**
     * @param Game $game
     * @param User $player
     * @return bool
     */
    public static function checkPlayerTurn(Game $game, User $player): bool
    {
        $lastMove = $game->moves()->orderBy('id','DESC')->first();

        // true if first move or last move with different player than current
        return !is_null($lastMove) ? ($lastMove->player_id != $player->id) : true;
    }

    /**
     * returns array like [X,O,-,O,X...]
     *
     * @param Game $game
     * @return array
     */
    public static function currentBoard(Game $game): array
    {

        $board = [];
        for($x=0;$x<9;$x++)
        {
            $move = $game->moves()->where('matrix_idx',($x+1))->first();
            $board[$x] = (!is_null($move)) ? (($move->player_id == $game->playerOne->id) ? $game->playerOneSymbol() : $game->playerTwoSymbol()) : '-' ;
        }

        return $board;

    }

    /**
     * @param Game $game
     * @param int $matrixIdx
     * @return bool
     */
    public static function checkMoveIsAvailable(Game $game, int $matrixIdx): bool
    {
        return is_null(($game->moves()->where('matrix_idx',$matrixIdx)->first()));
    }

    /**
     * @param Game $game
     * @return int
     */
    public static function getWinnerPlayerId(Game $game): int
    {
        return (count(self::getWinnerCombination($game,$game->player_one_id)) ? $game->player_one_id : (count(self::getWinnerCombination($game,$game->player_two_id))
                    ? $game->player_two_id
                    : 0));
    }

    /**
     * @param Game $game
     * @param int|null $playerId
     * @return array
     */
    private static function getWinnerCombination(Game $game, ?int $playerId = null): array
    {
        if(is_null($playerId))
            return [];

        $playerMoves = $game->moves()->where('player_id',$playerId)->get()->pluck('matrix_idx')->toArray();

        return array_filter(self::$matrixIdxsCombinationToWin,function($combination)use($playerMoves){
            return !array_diff($combination, $playerMoves);
        });
    }

    /**
     * @param Game $game
     * @return array
     */
    public static function statistics(Game $game): array
    {
        return [
            'FraS' => $game->status,
            'player_winner_id' => GameService::getWinnerPlayerId($game),
            'current_board' => GameService::currentBoard($game)
        ];
    }



}