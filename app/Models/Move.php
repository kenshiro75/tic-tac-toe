<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Move extends Model
{
    protected $table = 'moves';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'matrix_idx',
        'player_id',
        'game_id',
    ];

    public function game()
    {
        return $this->belongsTo(Game::class,'game_id','id');
    }

    public function player()
    {
        return $this->belongsTo(User::class,'player_id','id');
    }

}
