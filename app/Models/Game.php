<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Game extends Model
{
    const OPEN = 0;
    const WON = 1;
    const DRAW = 2;
    const ERROR = 3;

    protected $table = 'games';



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'status',
        'player_one_id',
        'player_two_id',
        'player_winner_id'
    ];

    public function moves()
    {
        return $this->hasMany(Move::class);
    }

    public function playerOne()
    {
        return $this->belongsTo(User::class,'player_one_id','id');
    }

    // source : https://en.wikipedia.org/wiki/Tic-tac-toe
    // There is no universally-agreed rule as to who plays first, but in this article the convention that X plays first is used.
    public function playerOneSymbol()
    {
        return 'X';
    }

    public function playerTwo()
    {
        return $this->belongsTo(User::class,'player_two_id','id');
    }

    public function playerTwoSymbol()
    {
        return 'O';
    }

    public function playerWinner()
    {
        return $this->belongsTo(User::class,'player_winner_id','id');
    }

}
