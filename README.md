# tic-tac-toe
Simple API project to play to TIC-TAC-TOE game.


## Project Setup

### Docker
this project is based on docker, it' s strongly recommended to install it before going on.

### Setup
1. create 2 db on your mysql local server
2. modify .env and .env.testing files in project root with your mysql credentials.
recommended : set APP_DEBUG=false if you plan to put the project online
3. modify /etc/hosts file adding this row
```
127.0.0.1 tictactoe.loc
```
get repo files, keep on "main" branch
```
git clone https://gitlab.com/kenshiro75/tic-tac-toe.git
cd tic-tac-toe
docker-compose build
docker-compose up -d
docker exec -ti tic-tac-toe bash
```

project refers to : 
http://tictactoe.loc:141/

## Seed
```
docker exec -ti tic-tac-toe bash
php artisan migrate:fresh --seed
```
you'll get 2 players (users)
1. player one : email":"p1@tictactoe.it", password":"123"
2. player two : email":"p2@tictactoe.it", password":"123"

## PHPUnit test
```
docker exec -ti tic-tac-toe bash
php artisan test --filter GameServiceTest
```

## Postman collection and environment
you can find Postman collection and env under : 
```
app/Http/Controllers/API/PostmanCollections
```

## API
all apis are under Bearer token Auth
### signin
http://tictactoe.loc:141/api/v1/auth/signin
request body: {
      	"email":"p1@tictactoe.it",
      	"password":"123"
      } 
response body: {
                   "status": "ok",
                   "bearer": "WDlgt7hHHYxG5Q767YAcsdcJd1SfwPB2nbiIbFXUWwCtQtsqvRT2feWckhyB"
               }    
### start
start a game
http://tictactoe.loc:141/api/v1/games/start
request body : empty
response body : {
                    "status": "ok",
                    "id": 1,
                    "player_symbol": "X"
                }

### move
play a move from a player
http://tictactoe.loc:141/api/v1/games/move
request body : {
    "game_id" : 1,
    "matrix_idx" : 6
}
response body : {
                    "status": "ok",
                    "game_statistics": {
                        "FraS": 0,
                        "player_winner_id": 0,
                        "current_board": [
                            "-",
                            "-",
                            "-",
                            "-",
                            "-",
                            "X",
                            "-",
                            "-",
                            "-"
                        ]
                    }
                }
 

## Support
write to kenshiro1@libero.it

## Authors and acknowledgment
Ettore Cirillo

## License
it's free for all
