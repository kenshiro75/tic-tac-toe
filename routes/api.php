<?php


use App\Http\Controllers\API\AnagController;
use App\Http\Controllers\API\AssetsController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CustomersController;
use App\Http\Controllers\API\GameController;
use App\Http\Controllers\API\OrdersController;
use App\Http\Controllers\API\ProductsController;
use App\Http\Controllers\API\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1'], function () {

    Route::group(['prefix'=>'auth'], function () {
        Route::post('signin', [AuthController::class, 'signin'])->name('auth.signin');
    });

    Route::group(['middleware' => ['auth:api']], function () {

        Route::group(['prefix'=>'games'], function () {
            Route::post('start', [GameController::class, 'start'])->name('game.start');
            Route::put('move', [GameController::class, 'move'])->name('game.move');
        });


    });

});