<?php


namespace Tests\Unit\Services;


use App\Models\Game;
use App\Models\Move;
use App\Models\User;
use App\Services\GameService;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class GameServiceTest extends TestCase
{
    use RefreshDatabase;

    protected Game $game;
    protected User $playerOne;
    protected User $playerTwo;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();

        $this->playerOne = User::where('email','p1@tictactoe.it')->first();
        $this->playerTwo = User::where('email','p2@tictactoe.it')->first();



    }

    /** @test */
    public function checkPlayerTest()
    {
        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->save();

        // test player 1 already in
        $this->assertTrue(GameService::checkPlayer($this->game, $this->playerOne));

        // test player 2 not in
        $this->assertTrue(GameService::checkPlayer($this->game, $this->playerTwo));

        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        $this->assertTrue(GameService::checkPlayer($this->game, $this->playerTwo));

        $playerThree = User::create([
            'name' => 'player',
            'surname' => 'three',
            'email' => 'p3@tictactoe.it',
            'password' => Hash::make('123'),
            'remember_token' => time(),
            'email_verified_at' => Carbon::now(),
            'active' => 1
        ]);

        // a 3rd player cannot play the game
        $this->assertFalse(GameService::checkPlayer($this->game, $playerThree));

        $this->game->delete();

    }

    /** @test */
    public function getGameStatusTest()
    {
        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        $this->assertEquals(GameService::getGameStatus($this->game), Game::OPEN);

        // add 9 moves without thinking at real idx -> Game::DRAW
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);

        $this->assertEquals(GameService::getGameStatus($this->game), Game::DRAW);

        // add 10th -> without thinking at real idx -> Game::ERROR
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>10, 'game_id'=>$this->game->id ]);
        $this->assertEquals(GameService::getGameStatus($this->game), Game::ERROR);

        $this->game->delete();
        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        // add 5 moves without thinking at real idx -> Game::DRAW
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>1, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>2, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>0, 'game_id'=>$this->game->id ]);
        $this->assertEquals(GameService::getGameStatus($this->game), Game::OPEN);

        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>3, 'game_id'=>$this->game->id ]);
        $this->assertEquals(GameService::getGameStatus($this->game), Game::WON);

        $this->game->delete();

    }

    /** @test */
    public function checkPlayerTurnTest()
    {

        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>1, 'game_id'=>$this->game->id ]);

        $this->assertTrue(GameService::checkPlayerTurn($this->game,$this->playerTwo));
        $this->assertFalse(GameService::checkPlayerTurn($this->game,$this->playerOne));

        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>2, 'game_id'=>$this->game->id ]);

        $this->assertTrue(GameService::checkPlayerTurn($this->game,$this->playerOne));
        $this->assertFalse(GameService::checkPlayerTurn($this->game,$this->playerTwo));

        $this->game->delete();

    }

    /** @test */
    public function currentBoardTest()
    {
        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        // add 9 moves without thinking at real idx -> Game::DRAW
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>1, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>2, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>3, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>4, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>5, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>6, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>7, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>8, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>9, 'game_id'=>$this->game->id ]);

        $this->assertEquals(GameService::currentBoard($this->game),['X','O','X','O','X','O','X','O','X']);

        $this->game->delete();

    }

    /** @test */
    public function checkMoveIsAvailableTest()
    {
        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        // add 9 moves without thinking at real idx -> Game::DRAW
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>1, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>2, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>3, 'game_id'=>$this->game->id ]);

        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>5, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>6, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>7, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>8, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>9, 'game_id'=>$this->game->id ]);

        $this->assertFalse(GameService::checkMoveIsAvailable($this->game,2));
        $this->assertTrue(GameService::checkMoveIsAvailable($this->game,4));

        $this->game->delete();
    }

    /** @test */
    public function getWinnerPlayerIdTest()
    {
        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        // add 9 moves without thinking at real idx -> Game::DRAW
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>1, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>4, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>2, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>8, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>3, 'game_id'=>$this->game->id ]);

        $this->assertEquals(GameService::getWinnerPlayerId($this->game),$this->playerOne->id);

        $this->game->delete();
    }

    /** @test */
    public function statisticsTest()
    {

        $this->game = new Game();
        $this->game->id = rand(1000,2000);
        $this->game->player_one_id = $this->playerOne->id;
        $this->game->player_two_id = $this->playerTwo->id;
        $this->game->save();

        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>1, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>7, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>2, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerTwo->id, 'matrix_idx'=>8, 'game_id'=>$this->game->id ]);
        Move::create(['player_id'=>$this->playerOne->id, 'matrix_idx'=>3, 'game_id'=>$this->game->id ]);

        $this->assertEquals(GameService::statistics($this->game)['player_winner_id'], $this->playerOne->id);
        $this->assertEquals(GameService::statistics($this->game)['current_board'], ['X','X','X','-','-','-','O','O','-']);

        $this->game->delete();

    }
}