<?php

namespace Database\Seeders;


use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // player one
        User::create([
            'name' => 'player',
            'surname' => 'one',
            'email' => 'p1@tictactoe.it',
            'password' => Hash::make('123'),
            'remember_token' => time(),
            'email_verified_at' => Carbon::now(),
            'active' => 1
        ]);

        // player two
        User::create([
            'name' => 'player',
            'surname' => 'two',
            'email' => 'p2@tictactoe.it',
            'password' => Hash::make('123'),
            'remember_token' => time(),
            'email_verified_at' => Carbon::now(),
            'active' => 1
        ]);
    }

}