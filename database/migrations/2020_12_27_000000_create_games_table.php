<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {

            $table->id();

            $table->integer('status')->default(0);

            $table->bigInteger('player_one_id')->unsigned();
            $table->foreign('player_one_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('player_two_id')->unsigned()->nullable();
            $table->foreign('player_two_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('player_winner_id')->unsigned()->nullable();
            $table->foreign('player_winner_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
