<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API
    |--------------------------------------------------------------------------
    |
    | list of specific api path to be interrogated
    |
    */

    'api' => [
        'clienti' => [
            'list' => 'clienti/lista',
            'new' => 'clienti/nuovo',
            'modify' => 'clienti/modifica',
            'delete' => 'clienti/elimina',
            'import' => 'clienti/importa',
        ],
        'fornitori' => [
            'list' => 'fornitori/lista',
            'new' => 'fornitori/nuovo',
            'modify' => 'fornitori/modifica',
            'delete' => 'fornitori/elimina',
            'import' => 'fornitori/importa',
        ],
        'prodotti' => [
            'list' => 'prodotti/lista',
            'new' => 'prodotti/nuovo',
            'modify' => 'prodotti/modifica',
            'delete' => 'prodotti/elimina',
            'import' => 'prodotti/importa',
        ],
        'preventivi' => [
            'list' => 'preventivi/lista',
            'new' => 'preventivi/nuovo',
            'modify' => 'preventivi/modifica',
            'delete' => 'preventivi/elimina',
            'details' => 'preventivi/dettagli'
        ],
        'ordini' => [
            'list' => 'ordini/lista',
            'new' => 'ordini/nuovo',
            'modify' => 'ordini/modifica',
            'delete' => 'ordini/elimina',
            'details' => 'ordini/dettagli',
        ],
        'fatture' => [
            'list' => 'fatture/lista',
            'new' => 'fatture/nuovo',
            'modify' => 'fatture/modifica',
            'delete' => 'fatture/elimina',
            'details' => 'fatture/dettagli',
        ],
        'tabelle' => [
            'info' => 'info/account',
            'service' => 'richiesta/info'
        ]
    ],


];
