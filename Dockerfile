FROM php:8.0-fpm

ARG ENV

RUN apt-get update -y \
    && apt-get install -y nginx git nano curl zlib1g-dev libpng-dev libsqlite3-dev libsqlite3-0 libzip-dev wget

# PHP_CPPFLAGS are used by the docker-php-ext-* scripts
ENV PHP_CPPFLAGS="$PHP_CPPFLAGS -std=c++11"

RUN docker-php-ext-install pdo_mysql \
    && docker-php-ext-install opcache \
    && apt-get install libicu-dev -y \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-install gd zip \
    && apt-get remove libicu-dev icu-devtools -y
RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=2'; \
        echo 'opcache.fast_shutdown=1'; \
        echo 'opcache.enable_cli=1'; \
    } > /usr/local/etc/php/conf.d/php-opocache-cfg.ini



RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# ========================================== XDEBUG ================================================
USER root
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug
RUN echo "xdebug.mode=develop,debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.client_host=host_internals" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.start_with_request=trigger" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.log=/tmp/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini
# ===================================================================================================

# TIMEZONE
RUN ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime && \
        apt-get -y update && \
        apt-get install -y tzdata && \
        dpkg-reconfigure --frontend noninteractive tzdata

# CRON
RUN apt-get update --allow-releaseinfo-change -y
RUN apt-get install cron -y
ADD ./docker_config/cron/cron.${ENV} /etc/cron.d/cron
RUN chmod 0644 /etc/cron.d/cron
RUN crontab /etc/cron.d/cron

# copy nginx conf
COPY ./docker_config/nginx/${ENV}.conf /etc/nginx/conf.d/default.conf

# copy entrypoint
COPY ./docker_config/env/entrypoint.sh /etc/entrypoint.sh

# copy files
COPY --chown=www-data:www-data . /var/www/html

# copy prod .env
COPY ./docker_config/env/${ENV}.env /var/www/html/.env
COPY ./docker_config/env/.env.testing /var/www/html/.env.testing

WORKDIR /var/www/html

RUN find /var/www/html -type d -exec chmod -R 555 {} \; \
    && find /var/www/html -type f -exec chmod -R 444 {} \; \
    && find /var/www/html/storage /var/www/html/bootstrap/cache -type d -exec chmod -R 777 {} \; \
    && find /var/www/html/storage /var/www/html/bootstrap/cache -type f -exec chmod -R 777 {} \;

RUN composer install --no-dev --optimize-autoloader
RUN php artisan migrate --force
RUN php artisan config:clear

EXPOSE 80

ENTRYPOINT ["sh", "/etc/entrypoint.sh"]
